.PHONY: clean

all: compile

compile: *.java
	javac $^
	@make doc

doc: *.java
	javadoc $^ -d ./documentation

clean: 
	rm *.class
	rm -rf ./documentation/*