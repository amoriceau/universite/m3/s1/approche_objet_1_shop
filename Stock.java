import java.util.ArrayList;

/*
 * Represent a Stock
 */
public class Stock {
    private  ArrayList<Product> products = new ArrayList<>();
    private String name;
    private String address;

    /**
     * @param products
     * @param name
     * @param address
     */
    public Stock(ArrayList<Product> products, String name, String address){
        this.address = address;
        this.name = name;
        this.products = products;
    }

    /**
     * @param name
     * @param address
     */
    public Stock(String name, String address){
        this.address = address;
        this.name = name;
    }

    // Getters -------------------------------------------------
    /**
     * @return
     */
    public String getAddress() {
        return address;
    }

    /**
     * @return
     */
    public String getName() {
        return name;
    }
    
    /**
     * @return
     */
    public ArrayList<Product> getProducts() {
        return products;
    }
    
    // Setters -------------------------------------------------
    /**
     * @param address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @param products
     */
    public void setProducts(ArrayList<Product> products) {
        this.products = products;
    }

    // Actions -------------------------------------------------
    // - Product Actions
    /**
     * @param product
     * @return
     */
    public boolean addProduct(Product product){
        return this.products.add(product);
    }

    /**
     * @param name
     * @return
     */
    public boolean removeProduct(String name) {
        if (name == null || name.isEmpty()) {
            // Handle the case where the provided name is null or empty
            System.out.println("Error: Product name cannot be null or empty.");
            return false;
        }
    
        for (Product product : products) {
            if (product.getName().equals(name)) {
                products.remove(product);
                System.out.println("Product '" + name + "' removed successfully.");
                return true;
            }
        }
    
        System.out.println("Error: Product '" + name + "' not found in the stock.");
        return false;
    }

    /**
     * @param name
     * @param quantity
     * @return
     */
    public boolean createProduct(String name, int quantity){
        return this.products.add(new Product(name, quantity));
    }

    /**
     * @param name
     * @return
     */
    public boolean hasProduct(String name) {
        return getProduct(name) != null;
    }

    /**
     * @param name
     * @return
     */
    public Product getProduct(String name){
        for (Product product : products) {
            if (product.getName().equals(name)) {
                return product;
            }
        }
        return null;
    }

    /**
     * @param name
     */
    public void displayProductInformation(String name) {
        if(name != null && !this.hasProduct(name)){
            System.out.println("The product " + name + " does not exist in this stock. (Looking into: " + this.name + ")");
        }

        if(name != null){
            System.out.println(this.name);
        }
        System.out.println("+--------------------------------------------------+--------------------------------------------------+--------------------------------------------------+");
        System.out.println("|                        UUID                      |                        Name                      |                     Quantity                     |");
        System.out.println("+--------------------------------------------------+--------------------------------------------------+--------------------------------------------------+");

        for (Product product : products) {
            if ((name == null) || (name != null && product.getName() != null && product.getName().equals(name))) {
                String formattedUuid = String.format("%-50s", product.getUuid());
                String formattedName = String.format("%-50s", product.getName());
                String formattedQuantity = String.format("%-50d", product.getQuantity());

                System.out.println("|" + formattedUuid + "|" + formattedName + "|" + formattedQuantity + "|");
            }
        }

        System.out.println("+--------------------------------------------------+--------------------------------------------------+--------------------------------------------------+\n");
    }

    /**
     * 
     */
    public void displayStockInformation(){
        System.out.println(this.name + " - Address: " + this.address + " - Inventory: " + this.products.size() + " items");
        this.displayProductInformation(null);
        System.out.println("\n");
    }

}
