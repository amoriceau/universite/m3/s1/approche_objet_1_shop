import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Log {
    private static final String LOG_FILE_EXTENSION = ".log";

    private static void writeLog(String level, String message, Throwable throwable) {
        try {
            String className = Thread.currentThread().getStackTrace()[3].getClassName();
            String logFileName = className + LOG_FILE_EXTENSION;
            PrintWriter writer = new PrintWriter(new FileWriter(logFileName, true));
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String timestamp = sdf.format(new Date());
            writer.println("[" + timestamp + "] [" + level + "] " + message);
            if (throwable != null) {             
                throwable.printStackTrace(writer);
            }
            writer.close();
        } catch (IOException e) {
            System.err.println("Error writing to log file: " + e.getMessage());
        }
    }

    public static void info(String message) {
        writeLog("INFO", message, null);
    }

    public static void error(String message) {
        writeLog("ERROR", message, null);
    }

    public static void error(String message, Throwable throwable) {
        writeLog("ERROR", message, throwable);
    }
}
