public enum ReturnCode {
    SUCCESS(0),
    FAILURE(1),
    INVALID_INPUT(2),
    CRITICAL_FAILURE(3);

    private final int code;

    ReturnCode(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}