# Docker env for java development

## Install

Have docker and docker compose on the machine, that's it.

## Run

`docker compose up -d`

#### Run a java command without attaching to the container:

`docker compose exec java [java|javac] path/to/file.java` etc...
