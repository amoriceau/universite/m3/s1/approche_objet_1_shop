import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/*
 * Represent a Shop
 * Also contain the main() of the program
 */
public class Shop {

    private static Map<String, Stock> stocks = new HashMap<String, Stock>();
    private static String fileName = "shop.csv";

    /**
     * Import stocks from a file and runs the program, it display a menu and allow
     * user to manage the shop
     * 
     * @param args
     */
    public static void main(String[] args) {
        importStocks();

        while (true) {
            int choice;
            Scanner input = new Scanner(System.in);

            try {
                System.out.println("What do you want to do");
                System.out.println("-------------------------\n");
                System.out.println("1 - Create stock/Add product to Stock");
                System.out.println("2 - Get product details");
                System.out.println("3 - Add/Remove products");
                System.out.println("4 - Display stocks");
                System.out.println("5 - Import a File");
                System.out.println("6 - Save the shop status");
                System.out.println("7 - Quit");

                choice = input.nextInt();

            } catch (Exception exception) {
                input.close();
                throw exception;
            }
            String filename;

            switch (choice) {
                case 1:
                    manageStocks(input);
                    break;
                case 2:
                    getProduct(input);
                    break;
                case 3:
                    manageProduct(input);
                    break;
                case 4:
                    displayStocks();
                    break;
                case 5:
                    System.out.println("Please provide the path to the file:");
                    System.out.println("-------------------------\n");
                    input.nextLine();
                    filename = input.nextLine();
                    System.out.println("Importing the file " + filename +"...\n");
                    if(importStocks(filename) != ReturnCode.SUCCESS){
                        System.out.println("Failed to import " + filename +"...\n");
                        Log.error("Failed to import " + filename);
                        break;
                    }else{
                        System.out.println("File " + filename + " have been imported\n");
                        Log.info("File " + filename + " have been imported");
                        displayStocks();
                        break;
                    }
                case 6:
                    System.out.println("Please provide the path for the save file:");
                    System.out.println("-------------------------\n");
                    input.nextLine();
                    filename = input.nextLine();
                    System.out.println("Creating the file " + filename +"...");
                    if(saveStocks(filename) != ReturnCode.SUCCESS){
                        System.out.println("Failed to save Shop data in " + filename +"...");
                        Log.error("Failed to save " + filename);
                        break;
                    }else{
                        System.out.println("File " + filename + " have been created");
                        Log.info("File " + filename + " have been created");
                        break;
                    }
                case 7:
                    System.out.println("Goodbye then.");
                    input.close();
                    return;
                default:
                    System.out.println("This is not a valid choice.\n\n");
                    break;
            }
        }
    }

    /**
     * Allow user to manage a product of the shop by displaying a menu
     * 
     * @param scanner
     */
    private static void manageProduct(Scanner scanner) {
        while (true) {
            System.out.println("\n\n\n");
            System.out.println("What do you want to do with the Products");
            System.out.println("-------------------------\n");
            System.out.println("1 - Add product to a stock");
            System.out.println("2 - Remove product from a stock");

            int choice = scanner.nextInt();

            switch (choice) {
                case 1:
                    addProductInStock(scanner);
                    return;
                case 2:
                    removeProductFromStock(scanner);
                    return;
                default:
                    System.out.println("Invalid command");
                    break;
            }

            break;
        }

        return;
    }

    /**
     * Return a product based on it's name, the name will be asked by the program to
     * the user
     * 
     * @param scanner
     */
    private static void getProduct(Scanner scanner) {
        if (stocks.size() == 0) {
            System.out.println("The shop does not have any stocks yet.");
            return;
        }

        ArrayList<Stock> existInStocks;
        while (true) {
            System.out.println("\n\n");
            System.out.println("What is the name of the product ?");

            scanner.nextLine();
            String choice = scanner.nextLine();
            existInStocks = findProductInStocks(choice);
            if (existInStocks != null) {
                System.out.println("\n" + choice + " exists in " + existInStocks.size() + " "
                        + (existInStocks.size() > 1 ? "stocks." : "stock.\n"));

                for (Stock stock : existInStocks) {
                    stock.displayProductInformation(choice);
                }

                return;
            }
            break;
        }
        return;
    }

    /**
     * Allow user to perform multiple actions on a stock
     * 
     * @param scanner
     */
    private static void manageStocks(Scanner scanner) {
        while (true) {
            System.out.println("\n\n\n");
            System.out.println("What do you want to do with the Stocks");
            System.out.println("-------------------------\n");
            System.out.println("1 - Create stock");
            System.out.println("2 - Add product to a stock");
            System.out.println("3 - Go back");

            int choice;
            try {
                choice = scanner.nextInt();
            } catch (Exception exception) {
                System.err.flush();
                choice = -1;
            }

            switch (choice) {
                case 1:
                    createStock(scanner);
                    break;
                case 2:
                    addProductInStock(scanner);
                    break;
                case 3:
                    System.out.println("\n\n");
                    return;
                default:
                    System.out.println("\nThis is not a valid choice.\n");
                    break;
            }
        }
    }

    /**
     * Create a stock in the shop
     * 
     * @param scanner
     */
    private static void createStock(Scanner scanner) {
        String stockName;
        while (true) {
            System.out.println("Please name the Stock");
            scanner.nextLine();
            stockName = scanner.nextLine().trim();
            if (stocks.get(stockName) != null) {
                System.out.println("A stock with this name already exists, provide another name.\n");
            } else {
                break;
            }
        }

        System.out.println("Please provide an address for the stock");
        String stockAddress = scanner.nextLine().trim();
        stocks.put(stockName, new Stock(stockName, stockAddress));

        displayStocks();

        return;
    }

    /**
     * Allow user to create a product in an existing stock
     * 
     * @param scanner
     */
    private static void addProductInStock(Scanner scanner) {
        System.out.println("What is the product to add");
        scanner.nextLine();
        String name = scanner.nextLine();
        System.out.println("How much to add in the stock ?");
        int quantity;

        try {
            quantity = scanner.nextInt();
        } catch (Exception exception) {
            quantity = 0;
        }

        boolean validStock = false;
        int stock = 1;

        while (!validStock) {
            System.out.println("\n\n--> Select one of the existing stocks (number)");
            displayStocks(true);

            try {
                stock = scanner.nextInt();
                if (stock < 1 || stock > stocks.size()) {
                    throw new Exception("Invalid Stock");
                }
                stock -= 1; // restore for index based calculation
                validStock = true;
            } catch (Exception exception) {
                System.out.println(exception.getMessage());
                Log.error("Scanner Error:\n", exception);
                break;
            }
        }

        if (getStock(stock).hasProduct(name)) {
            getStock(stock).getProduct(name).setQuantity(quantity);
            System.out.println(
                    "This product already exists in the given stock, the previous quantity have been replaced with the given one.");
        } else {
            if (!getStock(stock).createProduct(name, quantity)) {
                System.out.println("Could not create the product " + name + " in the desired stock.\n");
            }
        }

    }

    /**
     * Allow user to remove a product from a stock based on it's name
     * 
     * @param scanner
     */
    private static void removeProductFromStock(Scanner scanner) {
        System.out.println("What is the product to delete");
        scanner.nextLine();
        String name = scanner.nextLine();
        ArrayList<Stock> existInStocks = findProductInStocks(name);

        if (existInStocks == null || existInStocks.size() == 0) {
            System.out.println("\nThis product does not exists.\n\n");
            return;
        }

        boolean validStock = false;
        int stock = 1;

        while (!validStock) {
            System.out.println("\n\n--> Select the stock you want to delete the product from: (number)");
            int index = 0;
            for (Stock inStock : existInStocks) {
                System.out
                        .println(++index + " - " + inStock.getName() + " (" + inStock.getProducts().size() + " items)");
            }

            try {
                stock = scanner.nextInt();
                if (stock < 1 || stock > stocks.size()) {
                    throw new Exception("Invalid Stock.\n");
                }
                stock -= 1; // restore for index based calculation
                validStock = true;
            } catch (Exception exception) {
                System.out.println(exception.getMessage());
                break;
            }
        }

        if (!existInStocks.get(stock).removeProduct(name)) {
            System.out.println("Could not remove the product from the stock.");
        } else {
            existInStocks.get(stock).displayStockInformation();
        }

    }

    /**
     * Displays each stock as a table containing information about the stock, the
     * stocks can be indexed to facilitate user input, in indexed mode only the
     * stock name will be displayed
     * 
     * @param indexed
     */
    public static void displayStocks(boolean indexed) {
        System.out.println("Existing stocks:\n");

        if (indexed) {
            int index = 0;
            for (String key : stocks.keySet()) {
                System.out.println(++index + " - " + key + " (" + stocks.get(key).getProducts().size() + " items)");
            }
        } else {
            for (Stock stock : stocks.values()) {
                stock.displayStockInformation();
            }
        }
    }

    /**
     * Display stocks in non-indexed mode
     */
    public static void displayStocks() {
        displayStocks(false);
    }

    /**
     * return the stock based on it's index in the stocks attribute
     * 
     * @param index
     * @return
     */
    public static Stock getStock(int index) {
        try {
            return stocks.get(stocks.keySet().toArray()[index]);
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
            Log.error("", exception);

            return null;
        }
    }

    /**
     * return the stock based on it's name in the stocks attribute
     * 
     * @param name
     * @return
     */
    public static Stock getStock(String name) {
        return stocks.get(name);
    }

    /**
     * Find a product based on it's name,
     * 
     * @param name
     * @return a list of stocks containing the product
     */
    private static ArrayList<Stock> findProductInStocks(String name) {
        if (name == null) {
            return null;
        }

        ArrayList<Stock> stockWithArticle = new ArrayList<Stock>();

        for (Stock stock : stocks.values()) {
            if (stock.hasProduct(name)) {
                stockWithArticle.add(stock);
            }
        }

        if (stockWithArticle.size() == 0) {
            return null;
        }

        return stockWithArticle.size() == 0 ? null : stockWithArticle;
    }

    /**
     * Import a CSV file formatted that should be formatted like this:
     * stock name,stock address,product 1 name,product 1 quantity,product 2 name
     * ....
     * check shop.csv for example
     */
    public static ReturnCode importStocks(String file) {
        String importFile = fileName;

        if(file != null){
            importFile = file;
        }

        try {
            BufferedReader reader = new BufferedReader(new FileReader(importFile));
            String line;
            while ((line = reader.readLine()) != null) {
                String[] parts = line.split(",");
                if (parts.length >= 3) {
                    String stockName = parts[0].trim();
                    String stockAddress = parts[1].trim();
                    Stock stock = new Stock(stockName, stockAddress);

                    for (int i = 2; i < parts.length; i += 2) {
                        String productName = parts[i].trim();
                        int productQuantity = Integer.parseInt(parts[i + 1].trim());
                        Product product = new Product(productName, productQuantity);
                        stock.addProduct(product);
                    }

                    stocks.put(stockName, stock);
                }
            }
            reader.close();
            return ReturnCode.SUCCESS;
        } catch (IOException e) {
            System.out.println("Error importing stocks.\n");
            Log.error("Error importing stocks;", e);

            return ReturnCode.FAILURE;
        }
    }

    /**
     * Create a CSV file to save the actual state of the application, the same file
     * can be imported later to continue the manipulations
     */
    public static ReturnCode saveStocks(String file) {
        String exportFile = fileName;

        if(file != null){
            exportFile = file;
        }
        StringBuilder line = new StringBuilder();
        try (FileWriter writer = new FileWriter(exportFile)) {
            for (Stock stock : stocks.values()) {
                line.append(stock.getName() + "," + stock.getAddress() + ",");

                for (Product product : stock.getProducts()) {
                    line.append(product.getName() + "," + Integer.toString(product.getQuantity()) + ",");
                }

                writer.write(line.toString()+"\n");
                line.setLength(0);
            }
            return ReturnCode.SUCCESS;
        } catch (IOException e) {
            System.out.println("Error saving stocks to CSV.\n");
            Log.error("Error saving stocks to CSV;", e);
            return ReturnCode.FAILURE;
        }
    }
    

    public static ReturnCode importStocks(){
        return importStocks(null);
    }

    public static ReturnCode saveStocks(){
        return saveStocks(null);
    }
}
