import java.util.UUID;

/*
 * Represent a Product
 */
public class Product {
    
    private String name;
    private int quantity;
    final private UUID uuid;

    /**
     * @param name
     * @param quantity
     */
    public Product(String name, int quantity){
        this.name = name;
        this.quantity = quantity;
        this.uuid = UUID.randomUUID();
    }

    /**
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * @return
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * @return
     */
    public UUID getUuid() {
        return uuid;
    }

    /**
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }
    
    /**
     * @param quantity
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "Product [name=" + name + ", quantity=" + quantity + ", uuid=" + uuid + "]";
    }
}
